package com.dd;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;

public class StrokeGradientDrawable {
    private int mStrokeWidth;
    private int mStrokeColor;

    private ShapeElement mGradientDrawable;

    public StrokeGradientDrawable(ShapeElement drawable) {
        mGradientDrawable = drawable;
    }

    public int getStrokeWidth() {
        return mStrokeWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        mStrokeWidth = strokeWidth;
        mGradientDrawable.setStroke(strokeWidth, RgbColor.fromArgbInt(getStrokeColor()));
    }

    public int getStrokeColor() {
        return mStrokeColor;
    }

    public void setStrokeColor(int strokeColor) {
        mStrokeColor = strokeColor;
        mGradientDrawable.setStroke(getStrokeWidth(), RgbColor.fromArgbInt(strokeColor));
    }

    public ShapeElement getGradientDrawable() {
        return mGradientDrawable;
    }
}
