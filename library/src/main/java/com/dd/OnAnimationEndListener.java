package com.dd;

public interface OnAnimationEndListener {
    public void onAnimationEnd();
}
