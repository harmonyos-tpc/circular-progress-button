package com.dd;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class CircularProgressDrawable extends Element {
    private float mSweepAngle;
    private float mStartAngle;
    private int mSize;
    private int mStrokeWidth;
    private int mStrokeColor;
    private Rect fBounds;

    public CircularProgressDrawable(int size, int strokeWidth, int strokeColor) {
        mSize = size;
        mStrokeWidth = strokeWidth;
        mStrokeColor = strokeColor;
        mStartAngle = -90;
        mSweepAngle = 0;
    }

    public void setSweepAngle(float sweepAngle) {
        mSweepAngle = sweepAngle;
    }

    public int getSize() {
        return mSize;
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        if (mPath == null) {
            mPath = new Path();
        }
        mPath.reset();
        mPath.addArc(getRect(), mStartAngle, mSweepAngle);
        mPath.offset(fBounds.left, fBounds.top);
        canvas.drawPath(mPath, createPaint());
    }

    public void onBoundsChange(Rect bounds) {
        fBounds = bounds;
    }

    @Override
    public void setAlpha(int alpha) {}

    private RectFloat mRectF;
    private Paint mPaint;
    private Path mPath;

    private RectFloat getRect() {
        if (mRectF == null) {
            int index = mStrokeWidth / 2;
            mRectF = new RectFloat(index, index, getSize() - index, getSize() - index);
        }
        return mRectF;
    }

    private Paint createPaint() {
        if (mPaint == null) {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeWidth(mStrokeWidth);
            mPaint.setColor(new Color(mStrokeColor));
        }

        return mPaint;
    }

    public void createNativePtr(Object obj) {}
}
