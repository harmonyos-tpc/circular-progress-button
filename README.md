
# **How to use?**

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:circular-progress-button:1.0.0'
}
```

``` xml
    <com.dd.CircularProgressButton
        ohos:text_alignment="center"
        ohos:text_size="15vp"
        ohos:text_color="white"
        ohos:height="64vp"
        ohos:width="196vp"
        ohos:id="$+id:circularButton1"
        app:cpb_cornerRadius="48vp"
        app:cpb_iconComplete="$media:ic_action_accept"
        app:cpb_iconError="$media:ic_action_cancel"
        app:cpb_textIdle="Upload"
        />
```

``` java
        CircularProgressButton.setProgress(progress);
```

# API:

## CircularProgressButton
**public boolean isIndeterminateProgressMode()**
- description: get progress mode, is indeterminate

**public void setIndeterminateProgressMode(boolean indeterminateProgressMode)**
- description: set progress mode,ture =  indeterminate

**public void setBackgroundCompat(Drawable drawable)**
- description: set background element 

**public void setProgress(int progress)**
- description: set progress 

**public int getProgress()**
- description: get progress 

**public void setBackgroundColor(int color)**
- description: set background color 

**public void setStrokeColor(int color)**
- description: set stroke color 

**public String getIdleText()**
- description: get idle text 

**public String getCompleteText()**
- description: get complete text 

**public String getErrorText()**
- description: get error text 

**public void setIdleText(String text)**
- description: set idle text 

**public void setCompleteText(String text)**
- description: set complete text 

**public void setErrorText(String text)**
- description: set error text 

# AttrSet

|name|format|description|
|:---:|:---:|:---:|
| cpb_textIdle | string | set idle text
| cpb_textComplete | string | set complete text
| cpb_textError | string | set error text
| cpb_textProgress | string | set progress text
| cpb_iconComplete | element | set complete icon
| cpb_iconError | element | set error text
| cpb_cornerRadius | dimension | set corner radius
| cpb_paddingProgress | dimension | set progress padding
| cpb_selectorIdle_pressed | color | set idle pressed color
| cpb_selectorIdle_focused | color | set idle focused color
| cpb_selectorIdle_disabled | color | set idle disabled color
| cpb_selectorIdle | color | set idle default color
| cpb_selectorComplete_pressed | color | set complete pressed color
| cpb_selectorComplete_focused | color | set complete focused color
| cpb_selectorComplete_disabled | color | set complete disabled color
| cpb_selectorComplete | color | set complete default color
| cpb_selectorError_pressed | color | set error pressed color
| cpb_selectorError_focused | color | set error focused color
| cpb_selectorError_disabled | color | set error disabled color
| cpb_selectorError | color | set error default color
| cpb_colorProgress | color | set progress color
| cpb_colorIndicator | color | set indicator color
| cpb_colorIndicatorBackground | color | set indicator background color