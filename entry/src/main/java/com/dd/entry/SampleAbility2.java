package com.dd.entry;

import com.dd.CircularProgressButton;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class SampleAbility2 extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_sample2);

        final CircularProgressButton circularButton1 =
                (CircularProgressButton) findComponentById(ResourceTable.Id_circularButton1);
        circularButton1.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (circularButton1.getProgress() == 0) {
                            simulateSuccessProgress(circularButton1);
                        } else {
                            circularButton1.setProgress(0);
                        }
                    }
                });

        final CircularProgressButton circularButton2 =
                (CircularProgressButton) findComponentById(ResourceTable.Id_circularButton2);
        circularButton2.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (circularButton2.getProgress() == 0) {
                            simulateErrorProgress(circularButton2);
                        } else {
                            circularButton2.setProgress(0);
                        }
                    }
                });
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        AnimatorValue widthAnimation = new AnimatorValue();
        widthAnimation.setDuration(2000);
        widthAnimation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        widthAnimation.setValueUpdateListener(
                new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        button.setProgress((int) (100 * v));
                    }
                });
        widthAnimation.start();
    }

    private void simulateErrorProgress(final CircularProgressButton button) {
        AnimatorValue widthAnimation = new AnimatorValue();
        widthAnimation.setDuration(2000);
        widthAnimation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        widthAnimation.setValueUpdateListener(
                new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        int value = (int) (100 * v);
                        button.setProgress(value);
                        if (value == 99) {
                            button.setProgress(-1);
                        }
                    }
                });
        widthAnimation.start();
    }
}
