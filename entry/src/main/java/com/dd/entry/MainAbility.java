package com.dd.entry;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text text1 = (Text) findComponentById(ResourceTable.Id_text1);
        Text text2 = (Text) findComponentById(ResourceTable.Id_text2);
        Text text3 = (Text) findComponentById(ResourceTable.Id_text3);
        Text text4 = (Text) findComponentById(ResourceTable.Id_text4);
        Text text5 = (Text) findComponentById(ResourceTable.Id_text5);
        text1.setClickedListener(
                component -> {
                    Intent intent15 = new Intent();
                    Operation operation = new Intent.OperationBuilder().withAction("action.sample1").build();
                    intent15.setOperation(operation);
                    startAbility(intent15);
                });
        text2.setClickedListener(
                component -> {
                    Intent intent14 = new Intent();
                    Operation operation = new Intent.OperationBuilder().withAction("action.sample2").build();
                    intent14.setOperation(operation);
                    startAbility(intent14);
                });
        text3.setClickedListener(
                component -> {
                    Intent intent13 = new Intent();
                    Operation operation = new Intent.OperationBuilder().withAction("action.sample3").build();
                    intent13.setOperation(operation);
                    startAbility(intent13);
                });
        text4.setClickedListener(
                component -> {
                    Intent intent1 = new Intent();
                    Operation operation = new Intent.OperationBuilder().withAction("action.sample4").build();
                    intent1.setOperation(operation);
                    startAbility(intent1);
                });

        text5.setClickedListener(
                component -> {
                    Intent intent12 = new Intent();
                    Operation operation = new Intent.OperationBuilder().withAction("action.sample5").build();
                    intent12.setOperation(operation);
                    startAbility(intent12);
                });
    }
}
