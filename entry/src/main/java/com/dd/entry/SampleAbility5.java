package com.dd.entry;

import com.dd.CircularProgressButton;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class SampleAbility5 extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_sample5);

        final CircularProgressButton circularButton1 =
                (CircularProgressButton) findComponentById(ResourceTable.Id_circularButton1);
        circularButton1.setIndeterminateProgressMode(true);
        circularButton1.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (circularButton1.getProgress() == 0) {
                            circularButton1.setProgress(50);
                        } else if (circularButton1.getProgress() == 100) {
                            circularButton1.setProgress(0);
                        } else {
                            circularButton1.setProgress(100);
                        }
                    }
                });

        final CircularProgressButton circularButton2 =
                (CircularProgressButton) findComponentById(ResourceTable.Id_circularButton2);
        circularButton2.setIndeterminateProgressMode(true);
        circularButton2.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (circularButton2.getProgress() == 0) {
                            circularButton2.setProgress(50);
                        } else if (circularButton2.getProgress() == -1) {
                            circularButton2.setProgress(0);
                        } else {
                            circularButton2.setProgress(-1);
                        }
                    }
                });
    }
}
